# aws-utils
Amazon Web Services useful scripts
## Start-Stop an Instance
Script name: start-stop-instance.py
Language: Python >=3.6
Aditional software: pip
Install
``
pip install boto
``
What do you need to execute the script
1. Create an application to get the aws_access_key_id and aws_secret_access_key keys.
2. Have the right permissions to access and start/stop instances.
2. Have the region that your instance is.
3. Have the instance id.
